import React from "react";
import { Link } from "react-router-dom";
import PostList from "../components/posts/PostList";

const Home: React.FC = () => {
  const qtyPosts = 4;
  return (
    <main className="container mx-auto mt-8">
      <h1 className="text-4xl font-bold mb-4">Welcome to the Blog</h1>
      <p className="text-lg mb-5">
        Explore our posts on various interesting topics! Can't find what you're
        looking for? Visit the "Posts" section in the menu to discover more.
      </p>
      <PostList qty={qtyPosts} />
      <p className="text-center">
        <Link
          to="/posts"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
        >
          View all posts
        </Link>
      </p>
    </main>
  );
};

export default Home;
