import React from "react";
import PostList from "../components/posts/PostList";

const Posts: React.FC = () => {
  return (
    <main className="container mx-auto mt-8">
      <h1 className="text-4xl font-bold mb-4">Posts</h1>
      <PostList />
    </main>
  );
};

export default Posts;
