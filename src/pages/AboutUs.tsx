import React from 'react';

const AboutUs: React.FC = () => {
  return (
    <main className="container mx-auto mt-8">
      <h1 className="text-4xl font-bold mb-4">About Us</h1>
      <p className="text-lg">
        Welcome to our blog! We are passionate about sharing knowledge and insights
        on a variety of topics. Our goal is to provide valuable and interesting content
        for our readers. Explore our posts and join us on this exciting journey!
      </p>
    </main>
  );
};

export default AboutUs;
