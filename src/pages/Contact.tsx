import React from 'react';

const Contact: React.FC = () => {
  return (
    <main className="container mx-auto mt-8">
      <h1 className="text-4xl font-bold mb-4">Contact Us</h1>
      <p className="text-lg">
        If you have any questions, suggestions, or just want to say hello,
        feel free to reach out to us. You can contact us via email at <a href="mailto:info@example.com">info@example.com</a>.
      </p>
    </main>
  );
};

export default Contact;
