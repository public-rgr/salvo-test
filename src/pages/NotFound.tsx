import React from 'react';

const NotFound: React.FC = () => {
  return (
    <main className="container mx-auto mt-8 text-center">
      <h1 className="text-4xl font-bold mb-4">404 - Not Found</h1>
      <p className="text-lg">
        Oops! The page you are looking for might be in another castle.
        Go back <a href="/">home</a>.
      </p>
    </main>
  );
};

export default NotFound;
