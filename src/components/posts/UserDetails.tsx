import React from "react";
import { useParams } from "react-router-dom";
import UserDetailsContent from "./UserDetailsContent";

const UserDetails: React.FC = () => {
  const { userId } = useParams<{ userId: string }>();

  if (userId === undefined) {
    return (
      <main className="container mx-auto mt-8">
        <p>userId undefined.</p>
      </main>
    );
  }

  const userIdNumber = parseInt(userId, 10);

  if (isNaN(userIdNumber)) {
    return (
      <main className="container mx-auto mt-8">
        <p>Invalid userId number.</p>
      </main>
    );
  }

  const userDetailsUrl = `https://jsonplaceholder.typicode.com/users/${userIdNumber}`;

  return <UserDetailsContent userDetailsUrl={userDetailsUrl} />;
};

export default UserDetails;
