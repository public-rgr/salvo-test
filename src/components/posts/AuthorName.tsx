import React, { memo } from "react";
import useApi from "../../hooks/useApi";

interface User {
  id: number;
  name: string;
}

interface AuthorNameProps {
  userId: number;
}

const AuthorName: React.FC<AuthorNameProps> = memo(({ userId }) => {
  const userUrl = `https://jsonplaceholder.typicode.com/users/${userId}`;
  const {
    data: user,
    loading: userLoading,
    error: userError,
  } = useApi<User>(userUrl);

  if (userLoading) {
    return <span>Loading author name...</span>;
  }

  if (userError) {
    return <span>Error loading author name: {userError}</span>;
  }

  return <span>{user?.name || "Author not found"}</span>;
});

export default AuthorName;
