import React from "react";
import useApi from "../../hooks/useApi";

interface UserDetails {
  id: number;
  name: string;
  username: string;
  email: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    };
  };
  phone: string;
  website: string;
  company: {
    name: string;
    catchPhrase: string;
    bs: string;
  };
}

const UserDetailsContent: React.FC<{ userDetailsUrl: string }> = ({
  userDetailsUrl,
}) => {
  const {
    data: userDetails,
    loading: userDetailsLoading,
    error: userDetailsError,
  } = useApi<UserDetails>(userDetailsUrl);

  if (userDetailsLoading) {
    return (
      <main className="container mx-auto mt-8">
        <p>Loading author...</p>
      </main>
    );
  }

  if (userDetailsError) {
    return (
      <main className="container mx-auto mt-8">
        <p>Error loading author details: {userDetailsError}</p>
      </main>
    );
  }

  return (
    <main className="container mx-auto mt-8">
      <h1 className="text-2xl font-bold mb-4">{userDetails?.name}</h1>
      <p>
        <strong>Email:</strong> {userDetails?.email}
      </p>
      <p>
        <strong>Address:</strong> {userDetails?.address.street},{" "}
        {userDetails?.address.suite}, {userDetails?.address.city},{" "}
        {userDetails?.address.zipcode}
      </p>
    </main>
  );
};

export default UserDetailsContent;