import React from 'react';
import useApi from '../../hooks/useApi';

interface Comment {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
}

interface PostCommentsProps {
  postId: number;
}

const PostComments: React.FC<PostCommentsProps> = ({ postId }) => {
  const commentsUrl = `https://jsonplaceholder.typicode.com/posts/${postId}/comments`;
  const { data: comments, loading: commentsLoading, error: commentsError } = useApi<Comment[]>(commentsUrl);

  if (commentsLoading) {
    return <p className="text-gray-700">Loading comments...</p>;
  }

  if (commentsError) {
    return <p className="text-red-500">Error loading comments: {commentsError}</p>;
  }

  return (
    <div>
      <h2 className="text-xl font-bold mb-4">Comments</h2>
      <ul className="list-disc pl-6">
        {comments?.map((comment) => (
          <li key={comment.id} className="mb-2">
            <p className="text-gray-800">
              <strong>{comment.name}</strong> - {comment.body}
            </p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default PostComments;
