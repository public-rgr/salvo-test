import React from "react";
import { Link as RouterLink } from "react-router-dom";
import useApi from "../../hooks/useApi";
import AuthorName from "./AuthorName";
import PostComments from "./PostComments";

interface PostItemContentProps {
  postId: number | undefined;
  postUrl: string;
}

interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

const PostItemContent: React.FC<PostItemContentProps> = ({
  postId,
  postUrl,
}) => {
  const {
    data: post,
    loading: postLoading,
    error: postError,
  } = useApi<Post>(postUrl);

  if (postLoading) {
    return (
      <main className="container mx-auto mt-8">
        <p>Loading post...</p>
      </main>
    );
  }

  if (postError) {
    return (
      <main className="container mx-auto mt-8">
        <p>Error loading post: {postError}</p>
      </main>
    );
  }

  return (
    <main className="container mx-auto mt-8">
      <h1 className="text-2xl font-bold mb-4">{post?.title}</h1>
      <p>{post?.body}</p>
      <p>
        <strong>Autor:</strong>{" "}
        <RouterLink
          to={`/user-details/${post?.userId}`}
          className="text-blue-500"
        >
          <AuthorName userId={post?.userId || 0} />
        </RouterLink>
      </p>
      <br />
      <PostComments postId={postId!} />
    </main>
  );
};

export default PostItemContent;
