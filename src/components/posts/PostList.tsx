import React, { memo } from "react";
import { Link } from "react-router-dom";
import useApi from "../../hooks/useApi";
import useTruncate from "../../hooks/useTruncate";
import AuthorName from "./AuthorName";
import { usePostsContext } from "../../context/usePostsContext";

interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

interface PostListProps {
  qty?: number;
  maxLengthBody?: number;
}

const PostList: React.FC<PostListProps> = memo(({ qty, maxLengthBody }) => {
  const postsUrl = "https://jsonplaceholder.typicode.com/posts";
  const { basePath } = usePostsContext();

  const {
    data: posts,
    loading: postsLoading,
    error: postsError,
  } = useApi<Post[]>(postsUrl);

  const maxLength = maxLengthBody ? maxLengthBody : 100;
  const truncate = useTruncate(maxLength);

  if (postsLoading) {
    return <p>Loading posts...</p>;
  }

  if (postsError) {
    return <p>Error loading posts: {postsError}</p>;
  }

  const truncatedPosts = posts?.map((post) => ({
    ...post,
    truncatedBody: truncate(post.body),
  }));

  const limitedPosts =
    qty && qty > 0 ? truncatedPosts?.slice(0, qty) : truncatedPosts;

  return (
    <div className="grid grid-cols-1 mb-5 md:grid-cols-2 lg:grid-cols-2 gap-4">
      {limitedPosts?.map((post) => (
        <Link key={post.id} to={`${basePath}/${post.id}`}>
          <article className="border p-4 rounded-md">
            <h2 className="text-xl font-bold mb-2">{post.title}</h2>
            <p>{post.truncatedBody}</p>
            <br />
            <p>
              <strong>
                <AuthorName userId={post.userId} />
              </strong>
            </p>
          </article>
        </Link>
      ))}
    </div>
  );
});

export default PostList;
