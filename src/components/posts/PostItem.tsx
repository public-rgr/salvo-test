import React from "react";
import { useParams } from "react-router-dom";
import PostItemContent from "./PostItemContent";

const PostItem: React.FC = () => {
  const { id } = useParams<{ id: string | undefined }>();
  const postId = id ? Number(id) : undefined;

  if (isNaN(postId!)) {
    return (
      <main className="container mx-auto mt-8">
        <p>Invalid Id.</p>
      </main>
    );
  }

  const postUrl = `https://jsonplaceholder.typicode.com/posts/${postId}`;

  return <PostItemContent postId={postId} postUrl={postUrl} />;
};

export default PostItem;
