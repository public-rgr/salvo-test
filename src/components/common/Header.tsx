import React from 'react';
import { Link } from 'react-router-dom';
import viteLogo from '/vite.svg'

const Header: React.FC = () => {
  return (
    <header className="bg-gray-800 p-4">
      <div className="container mx-auto flex items-center justify-between">
        <div className="text-white text-lg font-bold">
          <Link to="/"><img src={viteLogo} className="logo" alt="Vite logo" /></Link>
        </div>
        <nav>
          <ul className="flex space-x-4">
            <li>
              <Link to="/posts" className="text-white">
                Posts
              </Link>
            </li>
            <li>
              <Link to="/about-us" className="text-white">
                About us
              </Link>
            </li>
            <li>
              <Link to="/contact" className="text-white">
                Contact
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
