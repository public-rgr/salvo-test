import React from 'react';

const Footer: React.FC = () => {
  return (
    <footer className="bg-gray-800 p-4 text-center text-white">
      <p>&copy; Copyright 2023.</p>
    </footer>
  );
};

export default Footer;
