import React, { createContext, ReactNode } from "react";

interface PostsContextProps {
  basePath: string;
}

const PostsContext = createContext<PostsContextProps | undefined>(undefined);

const PostsProvider: React.FC<{ basePath: string; children: ReactNode }> = (
  props
) => {
  const { basePath, children } = props;

  return (
    <PostsContext.Provider value={{ basePath: basePath! }}>
      {children}
    </PostsContext.Provider>
  );
};

export { PostsProvider };
export default PostsContext;
