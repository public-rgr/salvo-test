import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { PostsProvider } from './context/PostsContext';
import Header from './components/common/Header';
import Footer from './components/common/Footer';
import Home from './pages/Home';
import Posts from './pages/Posts';
import AboutUs from './pages/AboutUs';
import Contact from './pages/Contact';
import NotFound from './pages/NotFound';
import PostItem from './components/posts/PostItem';
import UserDetails from './components/posts/UserDetails';
import './App.css'

const App: React.FC = () => {
  const basePath = '/posts';

  return (
    <Router>
      <PostsProvider basePath={basePath}>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/posts" element={<Posts />} />
          <Route path="/about-us" element={<AboutUs />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/posts/:id" element={<PostItem />} />
          <Route path="/user-details/:userId" element={<UserDetails />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
        <Footer />
      </PostsProvider>
    </Router>
  );
};

export default App;
