import { useMemo } from "react";

const useTruncate = (maxLength: number) => {
  return useMemo(
    () => (text: string) => {
      if (text.length > maxLength) {
        return text.substring(0, maxLength - 3) + "...";
      }
      return text;
    },
    [maxLength]
  );
};

export default useTruncate;
