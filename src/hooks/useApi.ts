import { useState, useEffect, useRef } from "react";
import axios, { AxiosResponse } from "axios";

interface ApiResponse<T> {
  data: T | null;
  loading: boolean;
  error: string | null;
}

const useApi = <T>(url: string) => {
  const effectRan = useRef(false);
  const [apiResponse, setApiResponse] = useState<ApiResponse<T>>({
    data: null,
    loading: true,
    error: null,
  });

  useEffect(() => {
    if (effectRan.current === false) {
      const fetchData = async () => {
        try {
          const response: AxiosResponse<T> = await axios.get(url);
          setApiResponse({ data: response.data, loading: false, error: null });
        } catch (error) {
          setApiResponse({
            data: null,
            loading: false,
            error: "Error al cargar los datos",
          });
        }
      };

      fetchData();

      return () => {
        effectRan.current = true;
      };
    }
  }, [url]);

  return apiResponse;
};

export default useApi;
